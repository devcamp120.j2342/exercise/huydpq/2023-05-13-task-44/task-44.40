/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading()


})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onPageLoading() {
    // call APi lấy danh sách khóa học
    getCoursesListClick();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// hàm call APi lấy danh sách khóa học
function getCoursesListClick() {
    $.ajax({
        url: "https://630890e4722029d9ddd245bc.mockapi.io/api/v1" + "/courses",
        type: "GET",
        success: function (paramData) {
            console.log(paramData);
            // load Data
            loadDataPopular(paramData)
            loadDataTrending(paramData)
        },
        error: function (paramErr) {
            console.log(paramErr.status)
        }
    })
}

// hàm load dữ liệu ra vùng Most popular
function loadDataPopular(paramPopular) {
    //  lọc dữ liệu 
    var vFilterPopular = paramPopular.filter( (item) => (item.isPopular == true))

    console.log(vFilterPopular)
    // hiển thị dữ liệu
    $("#div-popular").empty()
    for(var bI = 0; bI < 4 ; bI ++){
    var bPopularAreaDiv = $("#div-popular");
    var bNewDiv = `<div class="card">
                        <img class="card-img-top" src="`+ vFilterPopular[bI].coverImage +`" alt="Card image cap">
                        <div class="card-body">
                            <h6 class="card-title text-primary">`+ vFilterPopular[bI].courseName +`</h6>
                            <p class="content-text"><i class="far fa-clock"></i>&nbsp;`+ vFilterPopular[bI].duration +` `+ vFilterPopular[bI].level +`</p>
                            <b>`+ "$"+ vFilterPopular[bI].discountPrice +` </b>
                        <del class="text-secondary">`+ "$" + vFilterPopular[bI].price +`</del>
                        </div>
                        <div class=" card-footer">
                             <div class="card-title m-0 d-flex align-items-center justify-content-between">
                            <div>
                                <img class="rounded-circle" src="`+ vFilterPopular[bI].teacherPhoto +`" alt="" height="35" width="35">
                                <small class="ml-2"><span class="mr-2">`+ vFilterPopular[bI].teacherName +`</span></small>
                            </div>
                             <i class="far fa-bookmark"></i>
                             </div>
                        </div>
                    </div>
    
    `
    bPopularAreaDiv.append(bNewDiv)
    }
}

// hàm load dữ liệu ra vùng Trending
function loadDataTrending(paramPopular) {
    //  lọc dữ liệu 
    var vFilterTrending = paramPopular.filter( (item) => (item.isTrending == true))
    
    console.log(vFilterTrending)
    // hiển thị dữ liệu
    $("#div-trending").empty()
    for(var bI = 0; bI < 4 ; bI ++){
    var bTrendingAreaDiv = $("#div-trending");
    var bNewDiv = `<div class="card">
                        <img class="card-img-top" src="`+ vFilterTrending[bI].coverImage +`" alt="Card image cap">
                        <div class="card-body">
                            <h6 class="card-title text-primary">`+ vFilterTrending[bI].courseName +`</h6>
                            <p class="content-text"><i class="far fa-clock"></i>&nbsp;`+ vFilterTrending[bI].duration +` `+ vFilterTrending[bI].level +`</p>
                            <b>`+ "$"+ vFilterTrending[bI].discountPrice +` </b>
                        <del class="text-secondary">`+ "$"+ vFilterTrending[bI].price +`</del>
                        </div>
                        <div class=" card-footer">
                             <div class="card-title m-0 d-flex align-items-center justify-content-between">
                            <div>
                                <img class="rounded-circle" src="`+ vFilterTrending[bI].teacherPhoto +`" alt="" height="35" width="35">
                                <small class="ml-2"><span class="mr-2">`+ vFilterTrending[bI].teacherName +`</span></small>
                            </div>
                             <i class="far fa-bookmark"></i>
                             </div>
                        </div>
                    </div>
    
    `
    bTrendingAreaDiv.append(bNewDiv)
    }
}

